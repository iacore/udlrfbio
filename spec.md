# udlrfbio

A simple interactive protocol.

This protocol assumes a stream-like (e.g. TCP, UART, SCTP, UDP+zmq) underlying transport. If the underlying transport has packet boundary, a command and its data MAY NOT cross such boundary (to ease parsing).

Here's an example OSI model, for illustration.

```
---------
  game™
---------
 udlrfbio
---------
   TCP
---------
```

## Terminology

### bytes™

a single byte of length, and that amount of bytes after.

Example: `05 68 65 6c 6c 6f` (5+hello)

### room™

Every room™ has its name as bytes™. You can think of it as a virtual location. 

A room™ SHOULD have same semantic meaning in a game™ no matter the time/where it is visited. In other words, the server SHOULD only store user data in the room™ name. Think of it as password (in early video games) if you will.

The server MUST NOT save extra user data apart from the room™ name.

### game™

A game running atop udlrfbio, which gives room names and movement meaning. See [https://git.envs.net/iacore/udlrfbio/wiki/Some-ideas](ideas.md) for ideas.

This application running atop udlrfbio may not be a game, but other applications (like telnet) instead.

## Commands

Every command starts with a byte, then some input. Then the server responds with some output. Both input and output may be nonexistent, denoted by `(none)` in the tables below.
```
HSB 765     4  3210 LSB
    ---  fdbk  move
```

The `---` bits are unused/reserved.

### `move` part (4 bits)

This part tells the server what the client wants to do

| hex | mnemonic | input data |
| - | - | - |
| 0 | noop | (none) |
| 1 | teleport | bytes™ |
| 8 | (move) up | (none) |
| 9 | (move) down | (none) |
| a | (move) left | (none) |
| b | (move) right | (none) |
| c | (move) forward | (none) |
| d | (move) backward | (none) |
| e | (move) in | (none) |
| f | (move) out | (none) |

On connect, the server may put the client in any room™. It may generate a random starting room™ for each player, or use the same for everyone.

On teleport, the server SHOULD disconnect if the target room is invalid (in terms of the game™), or move the client to that room. Teleporting to a previously visited room SHOULD be valid (unless the game logic has updated). This way, users can resume from the room they left off yesterday, last week, or last year.

On move commands, the server SHOULD always suceed in doing so. It may move the client to a new room, or move to current room (same as not move).

Moving the same direction from the same room MAY end you up in different rooms. The server may use this to load balance players into different regions.

Moving up then down and you may end up in a different room. The 8 moving directions are called up, down, etc, but they may represent different actions. Do not be bound by 4-Euclidean geometry!

### `fdbk` part (1 bit)

This part tells the server what to respond. Ideally, any command here SHOULD NOT change the current room™.

| hex | mnemonic | output data |
| - | - | - |
| 0 | noop | (none) |
| 1 | where (show current room name) | bytes™ |

## Example Session

This is a game™, where the server acts as a counter. When first connected, the room name is zero. up/down inc/decrement the room name.

```mermaid
sequenceDiagram
participant c as client
participant s as server

Note over c, s: TCP connect

c->>+s: 10 | where
s->>-c: 01 00 | "\x00"

c->>+s: 18 | up-where
s->>-c: 01 01 | "\x01"

c->>s: 08 | up
c->>s: 08 | up
c->>s: 08 | up
c->>+s: 18 | up-where
s->>-c: 01 05 | "\x05"

c->>+s: 11 01 00 | teleport-where "\x00"
s->>-c: 01 00 | "\x00"

Note over c, s: TCP disconnect
```

Remember, TCP is duplex, so the client can move around and ask for several locations in batch, before the server can respond.

```mermaid
sequenceDiagram
participant c as client
participant s as server

c->>s: 08 08 08 18 | up, up, up, up-where
c->>s: 08 08 08 18 | up, up, up, up-where
s->>c: 01 04 | "\x04"
s->>c: 01 08 | "\x08"
```

## Glossary

### Connection Initiation

Delegated to the underlying transport. You should be able to connect using `ncat`, although you may not type in the commands directly since this protocol doesn't use ASCII.

### Connection Abort

Delegated to the underlying transport. Either side may disassociate when they lose interest (or encounter protocol incompliance), like humans.

### Handling protocol incompliance

If the server sees invalid command, it should disconnect the underlying transport.

Before that, it may dump some error text as bytes™ before disconnecting. The client should abort on invalid response too, and it MAY show this text to the user.

This protocol is meant to be automated, so do not expect response invalid command.

### Protocol version negotiation

There is none at this protocol layer. See [https://git.envs.net/iacore/udlrfbio/wiki/Some-ideas](ideas.md#version) for example implementation.
