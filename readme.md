# udlrfbio

A simple interactive protocol.

Because it's so simple,  
You are encouraged to write your own client.  
You are encouraged to write your own server.  

[read the spec](spec.md)  
[read and write ideas on the conventions wiki](https://git.envs.net/iacore/udlrfbio/wiki/)
